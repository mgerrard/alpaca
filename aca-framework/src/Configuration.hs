module Configuration where

data Configuration = Configuration
  { fileParam :: String
  , debugParam :: String
  , timeoutParam :: Int
  , portfolioParam :: String
  , statisticsParam :: Bool
  , cscParam :: String
  , earlyExitParam :: Bool
  , generalizeTimeoutParam :: Int
  , blockValidPathsParam :: Bool
  , exitStrategyParam :: String
  , modularParam :: String
  , sequentializeParam :: Bool
  , genExitStratParam :: String
  , prefixParam :: String
  , targetFunctionParam :: String
  , octagonalizeParam :: Bool
  , partitionBoundParam :: Int
  , mergeLengthParam :: Int
  , genStratParam :: String
  , cppParam :: String
  , initTimeoutParam :: Int
  , excludeParam :: String
  } deriving (Show)

data CountConfiguration = CountConfiguration { cscFileToCount :: String }
