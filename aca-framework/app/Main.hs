module Main where

import Lib

import Options.Applicative
import Data.Semigroup ((<>))
import System.Exit
import System.Posix.Signals
import Control.Concurrent

main :: IO ()
main = do
  tid <- myThreadId
  _ <- installHandler keyboardSignal (Catch (killThread tid)) Nothing
  _ <- runAca =<< execParser opts
  killThread tid
  return ()

opts :: ParserInfo Configuration
opts = info ( configuration <**> helper )
  (  fullDesc
  <> header   "ALPACA -- A Large Portfolio-based Alternating Conditional Analysis" )

configuration :: Parser Configuration
configuration = Configuration
  <$> argument str
      (  metavar "FILE" )
  <*> strOption
      (  long "debug"
      <> short 'd'
      <> showDefault
      <> metavar "LEVEL"
      <> value "quiet"
      <> help "Print debugging statements at LEVEL of \
              \ ( full | slice | analyzers | direct | generalize | quiet )" )
  <*> option auto
      (  long "timeout"
      <> short 't'
      <> help "Set global timeout (in seconds) for analysis tools *after* first iteration (see --init-timeout flag)"
      <> showDefault
      <> value 900
      <> metavar "INT" )
  <*> strOption
      (  long "portfolio"
      <> short 'p'
      <> showDefault
      <> metavar "FILTER"
      <> value "all"
      <> help "Selectively run analyzers in porfolio given a FILTER of \
             \ ( all | cpaSeq | cpaBamBnb | cpaBamSlicing | interpChecker | \
             \ uAutomizer | uKojak | uTaipan | veriAbs | cbmc | twoLs | \
             \ depthK | esbmcIncr | esbmcKind | symbiotic | pesco | \
             \ cpaSeq16 | seahorn | smack | civl | (any comma-separated string of tools)")
  <*> switch
      (  long "statistics"
      <> short 's'
      <> help "Print statistics" )
  <*> strOption
      (  long "csc"
      <> showDefault
      <> metavar "CSC_FILE"
      <> value ""
      <> help "Pass in partial CSC; used for debugging" )
  <*> switch
      (  long "early-exit"
      <> help "Exit after first iteration. Used for testing efficacy of VERIFIER+CIVL." )
  <*> option auto
      (  long "generalize-timeout"
      <> help "Set global timeout (in seconds) for generalization phase of analysis tools"
      <> showDefault
      <> value 300
      <> metavar "INT" )
  <*> switch
      (  long "block-valid-paths"
      <> help "Block spurious error paths reported by some analysis tool." )
  <*> strOption
      (  long "exit-strategy"
      <> showDefault
      <> value "eager"
      <> metavar "STRATEGY"
      <> help "On each iteration of ALPACA, do you want to stop running the portfolio \
              \ run when the first valid error is confirmed, or wait until all \
              \ analyzers have either completed or exhausted their timebound? \
              \ Choose from a STRATEGY of ( eager | patient )" )
  <*> strOption
      (  long "modular"
      <> short 'm'
      <> showDefault
      <> metavar "FUNCTION"
      <> value "main"
      <> help "Run ALPACA only over a given function." )
  <*> switch
      (  long "sequentialize"
      <> help "Run tools in portfolio in sequence. Used for comparing timing statistics \
              \ with parallelization." )
  <*> strOption
      (  long "gen-exit-strategy"
      <> showDefault
      <> value "eager"
      <> metavar "GEN-STRATEGY"
      <> help "When ALPACA is generalizing, do you want to stop after finding at least one \
              \ successful blocking clause (eager), or do you want to try them all (patient)?" )
  <*> strOption
      (  long "prefix"
      <> showDefault
      <> metavar "PATH"
      <> value "."
      <> help "Specify the prefix to the '/logs_aca/' directory." )
  <*> strOption
      (  long "target-function"
      <> showDefault
      <> metavar "FUNCTION"
      <> value "__VERIFIER_error"
      <> help "The name of the function whose reachability you'd like to characterize." )
  <*> switch
      (  long "octagonalize"
      <> short 'o'
      <> help "Octagonalize constraints containing one or two variables." )
  <*> option auto
      (  long "partition-bound"
      <> help "Set number of maximum partitions before applying iteration-widening."
      <> showDefault
      <> value 4
      <> metavar "INT" )
  <*> option auto
      (  long "merge-length"
      <> help "Set number of partitions you would like to merge when applying iteration-widening. Must be >= partition-bound."
      <> showDefault
      <> value 2
      <> metavar "INT" )
  <*> strOption
      (  long "generalize-strategy"
      <> showDefault
      <> value "pessimisticEq"
      <> metavar "STRATEGY"
      <> help "When the CSC needs to be generalized, do you want to generalize \
              \ from the top of the logical lattice and prefer blocking equality \
              \ constraints first (pessimisticEq) or disequality constraints \
              \ (pessimisticDisEq); or do a full binary search on the lattice \
              \ with the respective variants (optimisticEq/DisEq)? \
              \ Choose from a STRATEGY of ( pessimisticEq | pessimisticDisEq | \
              \ optimisticEq | optimisticDisEq )" )
  <*> strOption
      (  long "cpp-flags"
      <> value ""
      <> help "Flags to pass to the C preprocessor (surround with double quotes if there are multiple arguments).")
  <*> option auto
      (  long "init-timeout"
      <> help "Set global timeout (in seconds) for initial iteration of analysis tools"
      <> showDefault
      <> value 900
      <> metavar "INT" )
  <*> strOption
      (  long "exclude"
      <> short 'e'
      <> showDefault
      <> metavar "FILTER"
      <> value ""
      <> help "Selectively exclude analyzers in porfolio")
