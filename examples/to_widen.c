extern void __VERIFIER_error() __attribute__ ((__noreturn__));
extern void __VERIFIER_assume(int);
extern int __VERIFIER_nondet_int();

int main () {
  int x = __VERIFIER_nondet_int();
  int y = __VERIFIER_nondet_int();

  for (int i=0; i<y; i++) {
    if (x == i) {
      __VERIFIER_error();
    }
  }
}
