/* 
 * This file is to test that modular ALPACA
 * will analyze functions called within another
 * function, e.g., if we call:
 *
 *   alpaca -m baz -p cpaSeq call_within_call.c
 *
 * then ALPACA will step into 'foo' as well.
 *
 */

extern void __VERIFIER_error() __attribute__ ((__noreturn__));
extern int __VERIFIER_nondet_int();

int foo (int x) {
  if (x > 0) {
    __VERIFIER_error();
  }
  return x;
}

int baz (int x) {
  x = foo(x);
  return x;
}

int main () {
  ;  
}
