extern void __VERIFIER_error() __attribute__ ((__noreturn__));
extern void __VERIFIER_assume(int);
extern int __VERIFIER_nondet_int();

int main () {
  int x;
  int i = 0;
  
  while (1) {
    x = __VERIFIER_nondet_int();
    if ((x > 0) && (i > 0)) {
      break;
    }
    i++;
  }

  if (x > 0) {
    __VERIFIER_error();
  }
}
