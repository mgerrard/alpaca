extern void __VERIFIER_error() __attribute__ ((__noreturn__));
extern void __VERIFIER_assume(int);
extern int __VERIFIER_nondet_int();

int main () {
  int x = __VERIFIER_nondet_int();
  if (__VERIFIER_nondet_int()) {
    __VERIFIER_error();
  }
  while (__VERIFIER_nondet_int()) {
    ;
  }
}
