extern void __VERIFIER_error() __attribute__ ((__noreturn__));
extern void __VERIFIER_assume(int);
extern int __VERIFIER_nondet_int();

int foo (int x, int y) {
  return x + 1;
}

int bar (int x) {
  return x + 2;
} 
  
int main () {
  int x = __VERIFIER_nondet_int();
  int y = __VERIFIER_nondet_int();
  int z;
  if (x < 0) {
    if (x == -20) {
      switch (foo(x,y)) {
      case 1: break;
      default:
	return 0;
      }
      //      x = foo(x);
    }
  } else {
    if (x == 3) {
      z = foo(x,y);
    }
  }
  return 0;
}
