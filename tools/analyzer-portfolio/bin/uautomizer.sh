#!/bin/bash

CURRENT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
source $CURRENT_DIR/../../../variables 

CONFIG="$(echo $1 | tr '[:upper:]' '[:lower:]')"
INPUT_FILE=$(readlink -e "$2")
OUTPUT_DIR=$(readlink -e "$3")


cd "$UA_ROOT"
export PATH="$PWD:$PATH" # make local z3/cvc4/mathsat binaries available to child processes
SVCOMP_SPEC="$PORTFOLIOS/PropertyUnreachCall.prp"

ARGS="--spec $SVCOMP_SPEC --architecture 32bit --file $INPUT_FILE --witness-dir $OUTPUT_DIR --witness-name witness.graphml"

case "$CONFIG" in
	ua)
		./Ultimate.py $ARGS
	;;
	*)
		echo "Unrecognized option: $1 (accepted values are 'ua')"
		exit 1
	;;
esac
