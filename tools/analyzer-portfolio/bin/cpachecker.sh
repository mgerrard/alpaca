#!/bin/bash

CURRENT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
source $CURRENT_DIR/../../../variables 

CONFIG="$(echo $1 | tr '[:upper:]' '[:lower:]')"
INPUT_FILE=$(readlink -e "$2")
OUTPUT_DIR=$(readlink -e "$3")

cd "$CPA_ROOT/scripts/"

SVCOMP_SPEC="$PORTFOLIOS/PropertyUnreachCall.prp"
ARGS="-timelimit $TIMEOUT -nolog -spec $SVCOMP_SPEC -outputpath $OUTPUT_DIR -setprop counterexample.export.graphml=witness.graphml $INPUT_FILE"

case "$CONFIG" in
	cpa16)
		./cpa.sh -svcomp16 $ARGS
	;;
	cpa17)
		./cpa.sh -svcomp17 $ARGS
	;;
	cpa18)
		./cpa.sh -svcomp18 $ARGS
	;;
	*)
		echo "Unrecognized option: $1 (accepted values are 'cpa16','cpa17','cpa18')"
		exit 1
	;;
esac
	
