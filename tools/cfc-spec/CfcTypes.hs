module CfcTypes where

type Domain = [InputVariable]
type InputVariable = (VariableName, VariableType)
type VariableName = String
type VariableType = String

type PartitionElement = (UpperBound, LowerBound)
type UpperBound = Formula
type LowerBound = [Formula]
type Formula = String

data Cfc = Cfc {
  cfcDomain :: Domain,
  cfcPartitions :: [PartitionElement]
  } deriving (Show)

