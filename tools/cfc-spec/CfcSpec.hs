module CfcSpec where

import Data.List.Split (splitOn)
import Data.List (dropWhileEnd)
import Data.Char (isSpace)
import Control.Exception.Base (assert)
import CfcTypes
import Lib

{-

This is a way to specify a Cfc 
mockup with a generalizing clause,
to test out logic for widening.
The specification language is:

<domain> ::= <input variable>*
<generalization> ::= <formula>
<input variable> ::= <id> <type>
<cfc> ::= <partition>*
<partition> ::= <upper bound> <lower bound>
<upper bound> ::= <formula>
<lower bound> ::= <formula>+
<formula> ::= <binary relation> "^" <formula>
            | <binary relation>
<binary relation> ::= <expr> <op> <expr>
<expr> ::= <digit> | <id>
<op> ::= "<" | ">" | "="
<digit> ::= {0..9}
<id> ::= {A..Z}
<type> ::= {"int"}

The file format is:

  <Comments>
  <Domain>
  <Generalization>
  <Cfc>

where each section is separated by linebreaks
and where the first line of each section begins
with some title which is thrown away (and all
comments are thrown away).

-}

createWideningMockup :: FilePath -> IO (Formula, Cfc)
createWideningMockup f = do
  spec <- readFile f
  let ss = splitOn linebreak spec
  {- Four sections : comments, domain, generalization, cfc -}
  assert ((length ss) == 4) return ()
  let domSection = lines $ ss !! 1
      genSection = lines $ ss !! 2
      cfcSection = lines $ ss !! 3
      -- Waste the headers
      domBody = tail domSection
      cfcBody = tail cfcSection
      genClause = makeZ3Expression $ trim $ last genSection
      domBody' = map trim domBody
      cfcBody' = map trim cfcBody

  domain <- mapM makeDomainElement domBody'
  let partStrs = filter (not . null) $ splitOn "Partition\n" (unlines cfcBody')
  partitions <- mapM makePartitionElement partStrs
  let cfc = Cfc domain partitions
  return (genClause, cfc)

makePartitionElement :: String -> IO PartitionElement
makePartitionElement s = do
  let bounds = filter (not . null) $ splitOn "Lower bound\n" s
      upper = last $ lines $ head bounds
      lower = lines $ last bounds
      upperBound = makeZ3Expression upper
      lowerBound = map makeZ3Expression lower
  return (upperBound, lowerBound)

makeDomainElement :: String -> IO InputVariable
makeDomainElement s = do
  let ss = splitOn " " s
  assert ((length ss) == 2) return ("","")
  let var = head ss
      ty = last ss
  return (var, ty)

linebreak = "\n\n"

