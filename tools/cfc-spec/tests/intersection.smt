(declare-fun X () Int)
(declare-fun Y () Int)
(assert (and (> X 1)(and (= Y 8)(< X 4))))
(check-sat)
