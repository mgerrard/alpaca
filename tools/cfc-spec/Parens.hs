module Parens where

-- pass in the string to be parsed, the parens stack, 
-- and the pair (leftParensIndex, currentIndex)
g :: String -> [Char] -> (Int, Int) -> [(Int, Int)]
g [] stack _ = []
g (')':xs) ['('] (left, curr) = (left, curr) : g xs [] ((-1), (curr+1))
g (')':xs) stack (left, curr) = g xs (tail stack) (left, (curr+1))
g ('(':xs) [] (left, curr) = g xs ['('] (curr, (curr+1))
g ('(':xs) stack (left, curr) = g xs ('(':stack) (left, (curr+1))
g (x:xs) stack (left, curr) = g xs stack (left,(curr+1))

outerParensIndices :: String -> [(Int, Int)]
outerParensIndices s = g s [] (0,0)

getOuterParensStrings :: String -> [String]
getOuterParensStrings s =
  let indices = outerParensIndices s
      substrings = map (\(start, end) -> substring start end s) indices
  in substrings

substring :: Int -> Int -> String -> String
substring start end text = take ((end+1) - start) (drop start text)
                      
