module Solver where

import Lib
import CfcTypes
import Data.String.Utils (rstrip, startswith)
import Data.List (find, intercalate)
import Control.Exception.Base (assert)
import System.Process (readProcess)

data SolverResult = TrueResult
                  | FalseResult
                  | UnknownResult
                  deriving (Eq)

widen :: Cfc -> Formula -> IO Cfc
widen cfc generalClause = do

  let d = cfcDomain cfc
      uppers = upperBounds cfc

  {- Intersections -}
  inResults <- mapM (intersects d generalClause) uppers
  let intersections = filterResultBy TrueResult inResults
      unknowns1 = filterResultBy UnknownResult inResults

  {- Implications -}
  imResults <- mapM (implies d generalClause) intersections
  let subsumed = filterResultBy TrueResult imResults
      partials = filterResultBy FalseResult imResults
      unknowns2 = filterResultBy UnknownResult imResults

      excludedFormulae = partials ++ unknowns1 ++ unknowns2
      safeGeneralClause = generalClause `minus` excludedFormulae

      {- Collect the children of the subsumed -}
      subsumptions = concat $ map (lowerBound cfc) subsumed
      assert (not (null subsumptions)) $ return cfc
      partition = (safeGeneralClause, subsumptions)
      cfc' = cfc `removeSubsumed` subsumed
      cfc'' = cfc' `addPartition` partition

  printCfc cfc''
  return cfc''

printCfc :: Cfc -> IO ()
printCfc cfc = do
  putStrLn "Cfc"
  putStrLn "  Partitions"
  mapM_ (putStrLn . showPartition) (cfcPartitions cfc)
  return ()

removeSubsumed :: Cfc -> [UpperBound] -> Cfc
removeSubsumed cfc [] = cfc
removeSubsumed cfc subsumed =
  let ps = cfcPartitions cfc
      ps' = filter (\(upper, _) -> not $ elem upper subsumed) ps
  in cfc { cfcPartitions = ps' }

lowerBound :: Cfc -> UpperBound -> LowerBound
lowerBound cfc u =
  let (Just (ub, lb)) = find (\(v, lb) -> u == v) (cfcPartitions cfc)
  in lb
  
intersects :: Domain -> Formula -> Formula -> IO (Formula, SolverResult)
intersects d gen up = do
  let domain = constructZ3Domain d
      {- Check if the intersection of two formulas
         has a satisfying model. -}
      assertion = "(assert (and "++gen++up++"))"
      condition = "(check-sat)"
      query = unlines $ domain++[assertion, condition]
  let queryFile = "intersection.smt"
  writeFile queryFile query
  result <- solve queryFile -- returns SolverResult
  if (result == Sat)
    then return (up, TrueResult)
    else if (result == Unsat)
      then return (up, FalseResult)
      else return (up, UnknownResult)

constructZ3Domain :: Domain -> [String]
constructZ3Domain d =
  let decls = map (\(vname, vtype) -> "(declare-fun "++vname++" () "++(getTypeString vtype)++")") d
  in decls

getTypeString :: VariableType -> String
getTypeString "int" = "Int"
getTypeString _ = assert False "No such type"

implies :: Domain -> Formula -> Formula -> IO (Formula, SolverResult)
implies d generalClause p = do
  let domain = constructZ3Domain d
      {- P implies Q, in Boolean logic, is: !P V Q -}
      implication = "(or (not"++p++")"++generalClause++")"
      {- To see if this implication is ever violated;
         ---i.e., it doesn't hold---we negate the
         formula and see if there's any satisfying
         model to disprove the implication. -}
      assertion = "(assert (not "++implication++"))"
      condition = "(check-sat)"
      query = unlines $ domain++[assertion, condition]
      queryFile = "implication.smt"

  writeFile queryFile query
  result <- solve queryFile -- returns SolverRawResult
  {- Notice that the below classification of the raw
     results from the solver differs from `intersects` -}
  if (result == Unsat)
    then return (p, TrueResult)
    else if (result == Sat)
      then return (p, FalseResult)
      else return (p, UnknownResult)
           
data SolverRawResult = Sat | Unsat | Unknown deriving (Eq, Show)

solve :: FilePath -> IO SolverRawResult
solve f = do
  out <- readProcess "/usr/bin/z3" ["-smt2", f] []
  let classification = classifyZ3Output (rstrip out)
  return classification

minus :: Formula -> [Formula] -> Formula
minus gen [] = gen
minus gen excluded =
  let negations = map (\c -> "(not "++c++")") excluded
      negatedConjunction = "(and "++(concat negations)++")"
  in
    if (length negations) == 1
      then "(and "++gen++(head negations)++")"
      else "(and "++gen++negatedConjunction++")"

classifyZ3Output :: String -> SolverRawResult
classifyZ3Output "sat" = Sat
classifyZ3Output "unsat" = Unsat
classifyZ3Output _ = Unknown

upperBounds :: Cfc -> [Formula]
upperBounds cfc = map fst $ cfcPartitions cfc

filterResultBy :: SolverResult -> [(Formula, SolverResult)] -> [Formula]
filterResultBy r (x:xs) | (snd x) == r = (fst x) : filterResultBy r xs
                        | otherwise    = filterResultBy r xs
filterResultBy _ []                    = []              

addPartition :: Cfc -> PartitionElement -> Cfc
addPartition cfc p =
  let ps = [p] ++ (cfcPartitions cfc)
  in cfc { cfcPartitions = ps }

