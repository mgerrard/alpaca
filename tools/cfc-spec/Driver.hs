import Lib
import Solver
import CfcSpec
import CfcTypes
import System.Environment (getArgs)

main = do
  args <- getArgs
  let testFile = head args
  (generalClause, cfc) <- createWideningMockup testFile
  widen cfc generalClause
  return ()
