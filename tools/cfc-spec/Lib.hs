module Lib where

import CfcTypes
import Data.List.Split (splitOn)
import Data.String.Utils (rstrip, startswith)
import Data.List (dropWhileEnd, find, intercalate)
import Data.Char (isSpace)

trim = dropWhileEnd isSpace . dropWhile isSpace

makeFormula = makeZ3Expression

makeZ3Expression :: String -> Formula
makeZ3Expression s =
  let clauseStrings = splitOn " ^ " s
      clauses = map binaryRelationExpr clauseStrings
  in
    if (length clauses) == 1
      then head clauses
      else "(and "++(concat clauses)++")"

binaryRelationExpr :: String -> String
binaryRelationExpr s =
  let tokens = splitOn " " s
      lhs = tokens !! 0
      op = tokens !! 1
      rhs = tokens !! 2
      expr = "("++op++" "++lhs++" "++rhs++")"
  in expr

-- pass in the string to be parsed, the parens stack, 
-- and the pair (leftParensIndex, currentIndex)
g :: String -> [Char] -> (Int, Int) -> [(Int, Int)]
g [] stack _ = []
g (')':xs) ['('] (left, curr) = (left, curr) : g xs [] ((-1), (curr+1))
g (')':xs) [] (left, curr) = g xs [] (left, (curr+1))
g (')':xs) stack (left, curr) = g xs (tail stack) (left, (curr+1))
g ('(':xs) [] (left, curr) = g xs ['('] (curr, (curr+1))
g ('(':xs) stack (left, curr) = g xs ('(':stack) (left, (curr+1))
g (x:xs) stack (left, curr) = g xs stack (left,(curr+1))

outerParensIndices :: String -> [(Int, Int)]
outerParensIndices s = g s [] (0,0)

outerClause :: String -> [String]
outerClause s =
  let indices = outerParensIndices s
      substrings = map (\(start, end) -> substring start end s) indices
  in substrings

substring :: Int -> Int -> String -> String
substring start end text = take ((end+1) - start) (drop start text)

showPartition :: PartitionElement -> String
showPartition p =
  let l1 = "    Upper bound"
      l2 = "      "++(z3ToSimple $ fst p)
      l3 = "    Lower bound"
      lns = map (\y -> "      "++(z3ToSimple y)) (snd p)
  in unlines $ [l1,l2,l3] ++ lns

z3ToSimple :: Formula -> Formula
z3ToSimple f
  | startswith "(and " f = intercalate " ^ " (map z3ToSimple (outerClause (drop 5 f)))
  | startswith "(not " f = "!("++(concat $ map z3ToSimple (outerClause (drop 5 f)))++")"
  | otherwise            = rewriteZ3Clause f

rewriteZ3Clause :: String -> String
rewriteZ3Clause s =
  let s' = trim s
      op = s' !! 1
      lhs = s' !! 3
      rhs = s' !! 5
  in [lhs,' ',op,' ',rhs]
