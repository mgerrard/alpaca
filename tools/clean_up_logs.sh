#!/bin/bash

# This script collates the log results
# from runs of ALPACA over multiple
# remote nodes

LOGS="logs_alpaca"

# Doppio servers
S1="mjg6v@doppio01.cs.virginia.edu"
S2="mjg6v@doppio02.cs.virginia.edu"
S3="mjg6v@doppio03.cs.virginia.edu"
S4="mjg6v@doppio04.cs.virginia.edu"
S5="mjg6v@doppio05.cs.virginia.edu"

for server in $S1 $S2 $S3 $S4 $S5;
do
    ssh $server <<EOF
      cd /localtmp
      rm -rf $LOGS/*/iter*
EOF
done

ssh $S1 <<EOF
  cd /u/mjg6v/alpaca-experiments/sv-benchmarks-svcomp19/c
  rm -rf CIVLREP
EOF
