#!/bin/bash
if [ $# -ne 2 ];
    then echo "Expected data file and metric name as parameters"; exit

fi

GP_FILE="/tmp/gnuplot.in"

sort -nr $1 >$1.sorted
echo "unset xtics" >/tmp/gnuplot.in
echo "set term pdfcairo dashed" >>$GP_FILE
echo "stats \"$1.sorted\" u 1:1 nooutput" >>$GP_FILE
echo "set xrange [*<-1:(STATS_records)<*]" >>$GP_FILE
echo "set yrange [*:(STATS_max_y+1)<*]" >>$GP_FILE
echo "set output \"$2.pdf\"" >>$GP_FILE
echo "plot \"$1.sorted\" with impulses lc 'black' title '$2'" >>$GP_FILE
gnuplot $GP_FILE
rm $1.sorted

