#!/usr/bin/env python3

import sys
import subprocess

# take in a file of candidates to check
candidates=sys.argv[1]

prefix="/home/mitch/work/experiments/sv-benchmarks-svcomp19/c/"
run_script="/home/mitch/work/alpaca/tools/run_until_prob_exceeded.sh"

# log files
rares="/tmp/candidates.rare"
fails = "/tmp/candidates.fail"
nonrares = "/tmp/candidates.nonrare"
# clear their contents
open(rares, 'w').close()
open(fails, 'w').close()
open(nonrares, 'w').close()

f = open(candidates, 'r')
list_of_candidates = f.read().splitlines()
 
for prog in list_of_candidates:
    # skip eca-rers programs; they will always be 'rare'
    # skip float programs; we do not support these currently
    if not (prog.startswith("eca-") or prog.startswith("float")):
        prog_path=prefix+prog
        try:
            # need to replace this with a Popen implementation to kill
            # the processes after they run
            subprocess.run([run_script,prog_path,fails,nonrares],timeout=20)
        except subprocess.TimeoutExpired:
            with open(rares, 'a') as file:
                file.write(prog_path+"\n")
f.close()

# Print stats
rare_count = len(open(rares).readlines(  ))
nonrare_count = len(open(nonrares).readlines(  ))
fail_count = len(open(fails).readlines(  ))
candidate_count = len(open(candidates).readlines(  ))

print("\nclassification of "+str(candidate_count)+" candidates:")
print("  rare: "+str(rare_count))
print("  nonrare: "+str(nonrare_count))
print("  fails: "+str(fail_count))
print("  skipped: "+str(candidate_count - (rare_count+nonrare_count+fail_count)))
print("")
