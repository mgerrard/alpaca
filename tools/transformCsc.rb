#!/usr/bin/ruby

def parse input
  chars = input.split('')
  stack = []; lowers = []; l=""
  chars.each do |c|
    case c
    when '['; stack.push c; l=l+c if stack.size>1
    when ']'; stack.pop; (stack.empty?) ? (lowers<<l.dup;l="") : (l=l+c)
    when ','; l=l+c if (not stack.empty?)
    else; l=l+c
    end
  end
  lowers
end

csc_file = ARGV[0]
csc = File.read(csc_file)

def collect_upper_bounds(s)
  uppers = s.split("upper = ["); uppers.shift; ups = []
  uppers.each do |u|
    up = u.split("], upperNegations").first
    up = up.split(",").join(" && ")
    ups << up
  end
  ups
end

def collect_lower_bounds(s)
  lowers = s.split("lowerBound = ["); lowers.shift; lows = []
  lowers.each do |l|
    ls = []
    low = l.split("], assumptions").first
    (parse low).each {|s| ls << s.split(",").join(" && ") }
    lows << ls
  end
  lows
end

ups = collect_upper_bounds(csc)
lows = collect_lower_bounds(csc)
partitions = []
ups.each_with_index do |u,i|
  partitions << [u,lows[i]]
end

partitions.each do |p|
  up=p[0]; lo=p[1]
  puts
  puts "  Partition:"
  puts "    Upper Bound:"
  puts "      "+up
  puts "    Lower Bound:"
  lo.each do |l|
    puts "      "+l
  end
end
