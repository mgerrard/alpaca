#!/usr/bin/env python3

import sys
import os
import subprocess
import math

prog=sys.argv[1]
prog_name=prog.rsplit('/', 1)[-1]
tmp="/tmp/"+prog_name+".tmp"
clean_prog="/tmp/"+prog_name+".c"
prog_exec="/tmp/"+prog_name+".out"

debug = False

failures = sys.argv[2]
nonrares = sys.argv[3]

##################################################################
## 1. Construct program with random inputs
##################################################################
#
header="""\
#include<stdlib.h>
#include<stdio.h>
#include<time.h>

void initializeRandomSeed (void) __attribute__ ((constructor));

void initializeRandomSeed (void) 
{ 
    // initialize the seed
    struct timespec nanos;
    clock_gettime(CLOCK_MONOTONIC, &nanos);
    srand(nanos.tv_nsec);
} 

void __VERIFIER_error() {
  exit(23);
}

void __VERIFIER_assume(int cond) {
  if (!(cond)) {
    exit(0);
  }
}

int __VERIFIER_nondet_int() {
  /* return 0 with 0.5 probability because
   * many SVCOMP programs test for equality
   * with zero
   */

  if (((rand() - (RAND_MAX/2)) % 2)) {
    return 0;
  } else {
    int r = rand() - (RAND_MAX/2);
    return r;
  }
}

short __VERIFIER_nondet_short() {
  /* return 0 with 0.5 probability because
   * many SVCOMP programs test for equality
   * with zero
   */

  if (((rand() - (RAND_MAX/2)) % 2)) {
    return 0;
  } else {
    int r = rand() - (RAND_MAX/2);
    return (r % 32767);
  }
}

unsigned int __VERIFIER_nondet_uint() {
  /* return 0 with 0.5 probability because
   * many SVCOMP programs test for equality
   * with zero
   */

  if (((rand() - (RAND_MAX/2)) % 2)) {
    return 0;
  } else {
    int r = rand() - (RAND_MAX/2);
    return r;
  }
}

char __VERIFIER_nondet_char(void) {
  int r = rand() - (RAND_MAX/2);
  return (r % 256);
}

unsigned char __VERIFIER_nondet_uchar(void) {
  int r = rand() - (RAND_MAX/2);
  return (r % 256);
}

_Bool __VERIFIER_nondet_bool(void) {
  /* return 0 with 0.5 probability because
   * many SVCOMP programs test for equality
   * with zero
   */

  if (((rand() - (RAND_MAX/2)) % 2)) {
    return 0;
  } else {
    int r = rand() - (RAND_MAX/2);
    return r;
  }
}

void * __VERIFIER_nondet_pointer(void) {
  int *r;
  r = rand() - (RAND_MAX/2);
  return r;
}

long __VERIFIER_nondet_long() {
  /* return 0 with 0.5 probability because
   * many SVCOMP programs test for equality
   * with zero
   */

  if (((rand() - (RAND_MAX/2)) % 2)) {
    return 0;
  } else {
    long r = rand() - (RAND_MAX/2);
    return r;
  }
}

unsigned long __VERIFIER_nondet_ulong() {
  /* return 0 with 0.5 probability because
   * many SVCOMP programs test for equality
   * with zero
   */

  if (((rand() - (RAND_MAX/2)) % 2)) {
    return 0;
  } else {
    long r = rand() - (RAND_MAX/2);
    return r;
  }
}

"""

# Add the above header to the original program
with open(tmp, 'w') as file:
        file.write(header)
subprocess.run(["cat "+prog+" >> "+tmp], shell=True)

# SVCOMP programs have a number of lines that redefine
# standard library definitions; we need to throw these out
bad_lines = ['typedef unsigned int size_t;',
             'extern  __attribute__((__nothrow__)) void *malloc(size_t __size )  __attribute__((__malloc__)) ;',
             'extern void *malloc(unsigned int sz );',
             'void *malloc(size_t size);',
             'extern void *malloc(unsigned int sz ) ;',
             'typedef unsigned short wchar_t;',
             'int snprintf(char * buf, size_t size, const char * fmt, ...);'
            ]

with open(tmp) as oldfile, open(clean_prog, 'w') as newfile:
    for line in oldfile:
        if not any(bad_line in line for bad_line in bad_lines):
            newfile.write(line)

# create the executable
FNULL = open(os.devnull, 'w')
gcc_exit_code = subprocess.run(["gcc -o "+prog_exec+" "+clean_prog], shell=True, stdout=FNULL, stderr=subprocess.STDOUT).returncode

if gcc_exit_code != 0:
    with open(failures, 'a') as file:
        file.write(prog+"\n")
    if debug:
        print("gcc failed on "+clean_prog+" with exit code: "+str(gcc_exit_code))
        print(" writing "+prog+" to "+failures)
    exit()

# delete tmp files
os.remove(tmp)
os.remove(clean_prog)

##################################################################
## 2. Launch sampling
##################################################################
#
# The sample average is #event/#samples,
# variance is average * (1-average) / #samples.
#
# Leave it running until the mean +/- (2 * stdev)
# is above or below the target probability.

samples=0
events=0
i=0
target_prob=0.001
minimum_runs=1000

while True:
    samples = samples + 1

    # run program and check if target
    # state is reached (exit code == 23)
    exit_code = subprocess.run([prog_exec], stdout=FNULL, stderr=subprocess.STDOUT).returncode
    if exit_code == 23:
        events = events + 1

    average = events / samples
    variance = average * (1 - average) / samples
    stdev = math.sqrt(variance)

    if debug:
        print("samples: "+str(samples))
        print("events: "+str(events))
        print("average: "+str(average))
        print("variance: "+str(variance))
        print("stdev: "+str(stdev))

    if i > minimum_runs:
       if (average - (2*stdev)) > target_prob:
           with open(nonrares, 'a') as file:
               file.write(prog+"\n")
           if debug:
               print(prog+" exceeds target probability.")
               print(" writing its name to "+nonrares)
       	   exit()
       
    i = i + 1
