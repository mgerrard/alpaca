import Statistics
import Data.List (sort, sortBy, zip4, unzip4)
import System.Process
import System.IO
import System.Environment (getArgs)
import System.FilePath.Glob

main = do
  args <- getArgs
  let baseDir = head args
  statFilePaths <- getStatFilePaths baseDir
  
  statFiles <- mapM readFile statFilePaths
  let stats = map (\y -> read y :: Statistics) statFiles

  let dts = map definiteTime stats
      pts = map possibleTime stats
      gts = map generalizeTime stats
      tts = map totalTime stats
      gcs = map generalizeCount stats
      iss = map iterationStat stats
      tps = map partitionCount stats
      eps = map exactPartitionCount stats
      timeQuadruple = zip4 tts dts pts gts
      sortedTimeQuad = reverse $ sortBy (\(t1, _, _, _) (t2, _, _, _) -> compare t1 t2) timeQuadruple
      (_, dts', pts', gts') = unzip4 sortedTimeQuad

  makePlot gcs "generalize_counts"
  makePlot iss "iteration_counts"
  makePartitionPlot tps eps
  makeTimePlot dts' pts' gts'

  return ()

makePlot :: (Show a) => [a] -> String -> IO ()
makePlot stats tag = do
  let fileName = tag ++ ".dat"
  writeFile fileName (unlines $ map show stats)
  let stdInputArgs = "./plotsorted.sh " ++ fileName ++ " " ++ tag
  readProcess "/bin/bash" [] stdInputArgs
  return ()

makePartitionPlot :: (Show a) => [a] -> [a] -> IO ()
makePartitionPlot tps eps = do
  let totalPartitions = "total_partitions.dat"
      partitionsWithoutGap = "partitions_without_gap.dat"
  writeFile totalPartitions (unlines $ map show tps)
  writeFile partitionsWithoutGap (unlines $ map show eps)
  let stdInputArgs = "./plotdisjointpartitions.sh " ++ totalPartitions ++ " " ++ partitionsWithoutGap
  readProcess "/bin/bash" [] stdInputArgs
  return ()

makeTimePlot :: (Show a, Num a) => [a] -> [a] -> [a] -> IO ()
makeTimePlot dts pts gts = do
  let genPossDef = "gen_poss_def.dat"
      genPoss = "gen_poss.dat"
      gen = "gen.dat"
      gpd = zipWith (+) dts $ zipWith (+) pts gts
      gp = zipWith (+) pts gts
      g = gts
  writeFile genPossDef (unlines $ map show gpd)
  writeFile genPoss (unlines $ map show gp)
  writeFile gen (unlines $ map show g)
  let stdInputArgs = "./plottimes.sh " ++ genPossDef ++ " " ++ genPoss ++ " " ++ gen
  readProcess "/bin/bash" [] stdInputArgs
  return ()

getStatFilePaths :: FilePath -> IO [FilePath]
getStatFilePaths baseDir = do
  let pat = compile "*/*.stats"
      dir = baseDir ++ "/logs_acf"
  globDir1 pat dir
