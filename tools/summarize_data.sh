#line 27 "summarize_data.nw"
#!/bin/bash

#line 52 "summarize_data.nw"
  S1="mjg6v@doppio01.cs.virginia.edu"
  S2="mjg6v@doppio02.cs.virginia.edu"
  S3="mjg6v@doppio03.cs.virginia.edu"
  S4="mjg6v@doppio04.cs.virginia.edu"
  S5="mjg6v@doppio05.cs.virginia.edu"

#line 87 "summarize_data.nw"
  LOG_DIR="/localtmp/logs_alpaca"
  BUCKET_DIR="/u/mjg6v/alpaca-experiments/buckets"
  ERROR_BUCKET_DIR="$BUCKET_DIR/error-reachability"
  FUNCTION_BUCKET_DIR="$BUCKET_DIR/function-reachability"

#line 296 "summarize_data.nw"
  INSTR_COPIES="/u/mjg6v/alpaca-experiments/sv-benchmarks-svcomp19/instrumented-reach"


#line 31 "summarize_data.nw"
# replace below with chunk "Collect file names in buckets"
#line 69 "summarize_data.nw"
  CURR_BUCKET_DIR=$FUNCTION_BUCKET_DIR
  PHASE="function-reach"
  
  
#line 323 "summarize_data.nw"
  ssh $S5 << EOF
  >"$CURR_BUCKET_DIR/domain"
  >"$CURR_BUCKET_DIR/timeout"
  >"$CURR_BUCKET_DIR/exact"
  >"$CURR_BUCKET_DIR/top"
  >"$CURR_BUCKET_DIR/single_gap"
  >"$CURR_BUCKET_DIR/multiple_gap"
EOF

#line 73 "summarize_data.nw"
#  for server in $S1 $S2 $S3 $S4 $S5;
  for server in $S5;
  do
    
#line 93 "summarize_data.nw"
  ssh $server << EOF
    cd $LOG_DIR
    
#line 164 "summarize_data.nw"
  domain_bucket=""
  success_bucket=""
  timeout_bucket=""
  exact_bucket=""
  gap_bucket=""
  non_top_single_bucket=""
  non_top_mult_bucket=""
  top_bucket=""

#line 96 "summarize_data.nw"
    
#line 125 "summarize_data.nw"
  
#line 148 "summarize_data.nw"
  if [[ $PHASE == "error-reach" ]]
  then
    PATTERN="*_false-unreach-call*"
  else
    PATTERN="*_reach_*"
  fi


#line 127 "summarize_data.nw"
  for f in \$PATTERN;
  do
    file="$LOG_DIR/\$f"
    if 
#line 201 "summarize_data.nw"
  compgen -G "\$file/final.csc"

#line 131 "summarize_data.nw"
    then
      if 
#line 184 "summarize_data.nw"
  grep X "\$file/final.csc"

#line 133 "summarize_data.nw"
      then
        success_bucket="\$success_bucket \$file"
      fi
    elif 
#line 204 "summarize_data.nw"
  compgen -G "\$file/*.csc"

#line 137 "summarize_data.nw"
    then
      if 
#line 156 "summarize_data.nw"
  [[ \`tail -1 \$file/portfolio.log\` == "Analysis moved to Top." ]]

#line 139 "summarize_data.nw"
      then
        top_bucket="\$top_bucket \$file"
      else
        timeout_bucket="\$timeout_bucket \$file"
      fi
    fi
  done

#line 97 "summarize_data.nw"
    
#line 364 "summarize_data.nw"
  for d in \$success_bucket; do
    run_summary=\`head -2 "\$d/exit.summary" | tail -1 \`
    if [ "\$run_summary" == "exact"  ]; then
      exact_bucket="\$exact_bucket \$d"
    fi    
  done

#line 98 "summarize_data.nw"
    
#line 379 "summarize_data.nw"
  for d in \$success_bucket; do
    run_summary=\`head -2 "\$d/exit.summary" | tail -1 \`
    if [ "\$run_summary" == "top"  ]; then
      top_bucket="\$top_bucket \$d"
    fi    
  done

#line 99 "summarize_data.nw"
    
#line 406 "summarize_data.nw"
  
#line 412 "summarize_data.nw"
  for d in \$success_bucket; do
    run_summary=\`head -2 "\$d/exit.summary" | tail -1 \`
    if [ "\$run_summary" == "gap"  ]; then
      gap_bucket="\$gap_bucket \$d"
    fi    
  done

#line 407 "summarize_data.nw"
  for f in \$gap_bucket; do
    
#line 420 "summarize_data.nw"
  csc_file="\$f/final.csc"
  read_upper=false
  gap_count=0
  
  while read LINE; do
    if [[ \$LINE == *"Upper Bound"* ]]; then
      read_upper=true
      continue
    elif [ "\$read_upper" = true ]; then
      upper=\$LINE
      read_upper=false
      continue
    elif [[ \$LINE == *"Lower Bound"* ]]; then
      read_lower=true
      continue
    elif [ "\$read_lower" = true ]; then
      lower=\$LINE
      read_lower=false
      if [[ \$upper != \$lower ]]; then
        let "gap_count=gap_count+1"
      fi
      upper=""; lower=""
    fi
  done < \$csc_file

  if [ \$gap_count -eq 1 ]; then
    non_top_single_bucket="\$f \$non_top_single_bucket"
  else
    non_top_mult_bucket="\$f \$non_top_mult_bucket"
  fi

#line 409 "summarize_data.nw"
  done

#line 100 "summarize_data.nw"
    
#line 207 "summarize_data.nw"
  if [[ $PHASE == "error-reach" ]]
  then
    
#line 307 "summarize_data.nw"
  
#line 341 "summarize_data.nw"
  for d in \$success_bucket; do
    f_name=\`head -1 "\$d/exit.summary"\`
    echo "\$f_name" >> "$ERROR_BUCKET_DIR/domain"
  done

  for d in \$timeout_bucket; do
    f_name=\`head -1 "\$d/exit.summary"\`
    echo "\$f_name" >> "$ERROR_BUCKET_DIR/domain"
  done

  for d in \$top_bucket; do
    f_name=\`head -1 "\$d/exit.summary"\`
    echo "\$f_name" >> "$ERROR_BUCKET_DIR/domain"
  done

#line 308 "summarize_data.nw"
  
#line 357 "summarize_data.nw"
  for d in \$timeout_bucket;
  do
    f_name=\`head -1 "\$d/exit.summary"\`
    echo "\$f_name" >> "$ERROR_BUCKET_DIR/timeout"
  done

#line 309 "summarize_data.nw"
  
#line 372 "summarize_data.nw"
  for d in \$exact_bucket;
  do
    f_name=\`head -1 "\$d/exit.summary"\`
    echo "\$f_name" >> "$ERROR_BUCKET_DIR/exact"
  done

#line 310 "summarize_data.nw"
  
#line 387 "summarize_data.nw"
  for d in \$top_bucket; do
    f_name=\`head -1 "\$d/exit.summary"\`
    echo "\$f_name" >> "$ERROR_BUCKET_DIR/top"
  done

#line 311 "summarize_data.nw"
  
#line 452 "summarize_data.nw"
  for d in \$non_top_single_bucket; do
    f_name=\`head -1 "\$d/exit.summary"\`
    echo "\$f_name" >> "$ERROR_BUCKET_DIR/single_gap"
  done

#line 312 "summarize_data.nw"
  
#line 458 "summarize_data.nw"
  for d in \$non_top_mult_bucket; do
    f_name=\`head -1 "\$d/exit.summary"\`
    echo "\$f_name" >> "$ERROR_BUCKET_DIR/multiple_gap"
  done


#line 210 "summarize_data.nw"
  else
    
#line 315 "summarize_data.nw"
  
#line 215 "summarize_data.nw"
  for d in \$success_bucket;
  do
    base=\`cut -d'/' -f4 <<<\$d\`
    
#line 293 "summarize_data.nw"
  instrumented_copy="\$d/iter.1/\$base.iter.1.c"

#line 219 "summarize_data.nw"
    
#line 299 "summarize_data.nw"
  repo_copy="$INSTR_COPIES/\$base.iter.1.c"

#line 220 "summarize_data.nw"
    
#line 302 "summarize_data.nw"
  if [ ! -f "\$repo_copy" ]; then
    cp "\$instrumented_copy" "\$repo_copy"
  fi

#line 221 "summarize_data.nw"
    echo "\$repo_copy" >> "$FUNCTION_BUCKET_DIR/domain"
  done

  for d in \$timeout_bucket;
  do
    base=\`cut -d'/' -f4 <<<\$d\`
    
#line 293 "summarize_data.nw"
  instrumented_copy="\$d/iter.1/\$base.iter.1.c"

#line 228 "summarize_data.nw"
    
#line 299 "summarize_data.nw"
  repo_copy="$INSTR_COPIES/\$base.iter.1.c"

#line 229 "summarize_data.nw"
    
#line 302 "summarize_data.nw"
  if [ ! -f "\$repo_copy" ]; then
    cp "\$instrumented_copy" "\$repo_copy"
  fi

#line 230 "summarize_data.nw"
    echo "\$repo_copy" >> "$FUNCTION_BUCKET_DIR/domain"
  done

  for d in \$top_bucket;
  do
    base=\`cut -d'/' -f4 <<<\$d\`
    
#line 293 "summarize_data.nw"
  instrumented_copy="\$d/iter.1/\$base.iter.1.c"

#line 237 "summarize_data.nw"
    
#line 299 "summarize_data.nw"
  repo_copy="$INSTR_COPIES/\$base.iter.1.c"

#line 238 "summarize_data.nw"
    
#line 302 "summarize_data.nw"
  if [ ! -f "\$repo_copy" ]; then
    cp "\$instrumented_copy" "\$repo_copy"
  fi

#line 239 "summarize_data.nw"
    echo "\$repo_copy" >> "$FUNCTION_BUCKET_DIR/domain"
  done

#line 316 "summarize_data.nw"
  
#line 243 "summarize_data.nw"
  for d in \$timeout_bucket;
  do
    base=\`cut -d'/' -f4 <<<\$d\`
    
#line 293 "summarize_data.nw"
  instrumented_copy="\$d/iter.1/\$base.iter.1.c"

#line 247 "summarize_data.nw"
    
#line 299 "summarize_data.nw"
  repo_copy="$INSTR_COPIES/\$base.iter.1.c"

#line 248 "summarize_data.nw"
    
#line 302 "summarize_data.nw"
  if [ ! -f "\$repo_copy" ]; then
    cp "\$instrumented_copy" "\$repo_copy"
  fi

#line 249 "summarize_data.nw"
    echo "\$repo_copy" >> "$FUNCTION_BUCKET_DIR/timeout"
  done

#line 317 "summarize_data.nw"
  
#line 283 "summarize_data.nw"
  for d in \$exact_bucket;
  do
    base=\`cut -d'/' -f4 <<<\$d\`
    
#line 293 "summarize_data.nw"
  instrumented_copy="\$d/iter.1/\$base.iter.1.c"

#line 287 "summarize_data.nw"
    
#line 299 "summarize_data.nw"
  repo_copy="$INSTR_COPIES/\$base.iter.1.c"

#line 288 "summarize_data.nw"
    
#line 302 "summarize_data.nw"
  if [ ! -f "\$repo_copy" ]; then
    cp "\$instrumented_copy" "\$repo_copy"
  fi

#line 289 "summarize_data.nw"
    echo "\$repo_copy" >> "$FUNCTION_BUCKET_DIR/exact"
  done

#line 318 "summarize_data.nw"
  
#line 253 "summarize_data.nw"
  for d in \$top_bucket;
  do
    base=\`cut -d'/' -f4 <<<\$d\`
    
#line 293 "summarize_data.nw"
  instrumented_copy="\$d/iter.1/\$base.iter.1.c"

#line 257 "summarize_data.nw"
    
#line 299 "summarize_data.nw"
  repo_copy="$INSTR_COPIES/\$base.iter.1.c"

#line 258 "summarize_data.nw"
    
#line 302 "summarize_data.nw"
  if [ ! -f "\$repo_copy" ]; then
    cp "\$instrumented_copy" "\$repo_copy"
  fi

#line 259 "summarize_data.nw"
    echo "\$repo_copy" >> "$FUNCTION_BUCKET_DIR/top"
  done

#line 319 "summarize_data.nw"
  
#line 263 "summarize_data.nw"
  for d in \$non_top_single_bucket;
  do
    base=\`cut -d'/' -f4 <<<\$d\`
    
#line 293 "summarize_data.nw"
  instrumented_copy="\$d/iter.1/\$base.iter.1.c"

#line 267 "summarize_data.nw"
    
#line 299 "summarize_data.nw"
  repo_copy="$INSTR_COPIES/\$base.iter.1.c"

#line 268 "summarize_data.nw"
    
#line 302 "summarize_data.nw"
  if [ ! -f "\$repo_copy" ]; then
    cp "\$instrumented_copy" "\$repo_copy"
  fi

#line 269 "summarize_data.nw"
    echo "\$repo_copy" >> "$FUNCTION_BUCKET_DIR/single_gap"
  done

#line 320 "summarize_data.nw"
  
#line 273 "summarize_data.nw"
  for d in \$non_top_mult_bucket;
  do
    base=\`cut -d'/' -f4 <<<\$d\`
    
#line 293 "summarize_data.nw"
  instrumented_copy="\$d/iter.1/\$base.iter.1.c"

#line 277 "summarize_data.nw"
    
#line 299 "summarize_data.nw"
  repo_copy="$INSTR_COPIES/\$base.iter.1.c"

#line 278 "summarize_data.nw"
    
#line 302 "summarize_data.nw"
  if [ ! -f "\$repo_copy" ]; then
    cp "\$instrumented_copy" "\$repo_copy"
  fi

#line 279 "summarize_data.nw"
    echo "\$repo_copy" >> "$FUNCTION_BUCKET_DIR/multiple_gap"
  done


#line 212 "summarize_data.nw"
  fi

#line 101 "summarize_data.nw"
EOF

#line 77 "summarize_data.nw"
  done

#line 464 "summarize_data.nw"
  
#line 467 "summarize_data.nw"
  ssh $S5 << EOF

  summary="$ERROR_BUCKET_DIR/summary"
  >"\$summary" # clear any existing summary file

  
#line 481 "summarize_data.nw"
  tag="domain"
  
#line 505 "summarize_data.nw"
  number=\`wc -l < $ERROR_BUCKET_DIR/\$tag\`

  MIN=-1
  MAX=0
  NUM=0
  SUM=0

  while read FILE; do
    let "NUM=NUM+1"
    SLOC=\`cloc --force-lang="C" --csv \$FILE | tail -1 | cut -d',' -f5\`
    
    if [ \$MIN -lt 0 ]; then
	MIN=\$SLOC
    elif [ \$SLOC -lt \$MIN ]; then
	MIN=\$SLOC
    fi

    if [ \$SLOC -gt \$MAX ]; then
	MAX=\$SLOC
    fi

    let "SUM=SUM+SLOC"
  done < $ERROR_BUCKET_DIR/\$tag

  if [ "\$NUM" -eq 0 ]; then
    AVG=0
  else
    let "AVG=SUM / NUM"
  fi

  if [ "\$MIN" -lt 0 ]; then
    MIN=0
  fi

  echo "\$tag: \$number (SLOC avg: \$AVG, max: \$MAX, min: \$MIN)">>"\$summary"

#line 473 "summarize_data.nw"
  
#line 485 "summarize_data.nw"
  tag="exact"
  
#line 505 "summarize_data.nw"
  number=\`wc -l < $ERROR_BUCKET_DIR/\$tag\`

  MIN=-1
  MAX=0
  NUM=0
  SUM=0

  while read FILE; do
    let "NUM=NUM+1"
    SLOC=\`cloc --force-lang="C" --csv \$FILE | tail -1 | cut -d',' -f5\`
    
    if [ \$MIN -lt 0 ]; then
	MIN=\$SLOC
    elif [ \$SLOC -lt \$MIN ]; then
	MIN=\$SLOC
    fi

    if [ \$SLOC -gt \$MAX ]; then
	MAX=\$SLOC
    fi

    let "SUM=SUM+SLOC"
  done < $ERROR_BUCKET_DIR/\$tag

  if [ "\$NUM" -eq 0 ]; then
    AVG=0
  else
    let "AVG=SUM / NUM"
  fi

  if [ "\$MIN" -lt 0 ]; then
    MIN=0
  fi

  echo "\$tag: \$number (SLOC avg: \$AVG, max: \$MAX, min: \$MIN)">>"\$summary"

#line 474 "summarize_data.nw"
  
#line 489 "summarize_data.nw"
  tag="top"
  
#line 505 "summarize_data.nw"
  number=\`wc -l < $ERROR_BUCKET_DIR/\$tag\`

  MIN=-1
  MAX=0
  NUM=0
  SUM=0

  while read FILE; do
    let "NUM=NUM+1"
    SLOC=\`cloc --force-lang="C" --csv \$FILE | tail -1 | cut -d',' -f5\`
    
    if [ \$MIN -lt 0 ]; then
	MIN=\$SLOC
    elif [ \$SLOC -lt \$MIN ]; then
	MIN=\$SLOC
    fi

    if [ \$SLOC -gt \$MAX ]; then
	MAX=\$SLOC
    fi

    let "SUM=SUM+SLOC"
  done < $ERROR_BUCKET_DIR/\$tag

  if [ "\$NUM" -eq 0 ]; then
    AVG=0
  else
    let "AVG=SUM / NUM"
  fi

  if [ "\$MIN" -lt 0 ]; then
    MIN=0
  fi

  echo "\$tag: \$number (SLOC avg: \$AVG, max: \$MAX, min: \$MIN)">>"\$summary"

#line 475 "summarize_data.nw"
  
#line 493 "summarize_data.nw"
  tag="single_gap"
  
#line 505 "summarize_data.nw"
  number=\`wc -l < $ERROR_BUCKET_DIR/\$tag\`

  MIN=-1
  MAX=0
  NUM=0
  SUM=0

  while read FILE; do
    let "NUM=NUM+1"
    SLOC=\`cloc --force-lang="C" --csv \$FILE | tail -1 | cut -d',' -f5\`
    
    if [ \$MIN -lt 0 ]; then
	MIN=\$SLOC
    elif [ \$SLOC -lt \$MIN ]; then
	MIN=\$SLOC
    fi

    if [ \$SLOC -gt \$MAX ]; then
	MAX=\$SLOC
    fi

    let "SUM=SUM+SLOC"
  done < $ERROR_BUCKET_DIR/\$tag

  if [ "\$NUM" -eq 0 ]; then
    AVG=0
  else
    let "AVG=SUM / NUM"
  fi

  if [ "\$MIN" -lt 0 ]; then
    MIN=0
  fi

  echo "\$tag: \$number (SLOC avg: \$AVG, max: \$MAX, min: \$MIN)">>"\$summary"

#line 476 "summarize_data.nw"
  
#line 497 "summarize_data.nw"
  tag="multiple_gap"
  
#line 505 "summarize_data.nw"
  number=\`wc -l < $ERROR_BUCKET_DIR/\$tag\`

  MIN=-1
  MAX=0
  NUM=0
  SUM=0

  while read FILE; do
    let "NUM=NUM+1"
    SLOC=\`cloc --force-lang="C" --csv \$FILE | tail -1 | cut -d',' -f5\`
    
    if [ \$MIN -lt 0 ]; then
	MIN=\$SLOC
    elif [ \$SLOC -lt \$MIN ]; then
	MIN=\$SLOC
    fi

    if [ \$SLOC -gt \$MAX ]; then
	MAX=\$SLOC
    fi

    let "SUM=SUM+SLOC"
  done < $ERROR_BUCKET_DIR/\$tag

  if [ "\$NUM" -eq 0 ]; then
    AVG=0
  else
    let "AVG=SUM / NUM"
  fi

  if [ "\$MIN" -lt 0 ]; then
    MIN=0
  fi

  echo "\$tag: \$number (SLOC avg: \$AVG, max: \$MAX, min: \$MIN)">>"\$summary"

#line 477 "summarize_data.nw"
  
#line 501 "summarize_data.nw"
  tag="timeout"
  
#line 505 "summarize_data.nw"
  number=\`wc -l < $ERROR_BUCKET_DIR/\$tag\`

  MIN=-1
  MAX=0
  NUM=0
  SUM=0

  while read FILE; do
    let "NUM=NUM+1"
    SLOC=\`cloc --force-lang="C" --csv \$FILE | tail -1 | cut -d',' -f5\`
    
    if [ \$MIN -lt 0 ]; then
	MIN=\$SLOC
    elif [ \$SLOC -lt \$MIN ]; then
	MIN=\$SLOC
    fi

    if [ \$SLOC -gt \$MAX ]; then
	MAX=\$SLOC
    fi

    let "SUM=SUM+SLOC"
  done < $ERROR_BUCKET_DIR/\$tag

  if [ "\$NUM" -eq 0 ]; then
    AVG=0
  else
    let "AVG=SUM / NUM"
  fi

  if [ "\$MIN" -lt 0 ]; then
    MIN=0
  fi

  echo "\$tag: \$number (SLOC avg: \$AVG, max: \$MAX, min: \$MIN)">>"\$summary"

#line 478 "summarize_data.nw"
EOF



