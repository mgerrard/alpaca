#!/usr/bin/env bash
if test "$BASH" = "" || "$BASH" -uc "a=();true \"\${a[@]}\"" 2>/dev/null; then
    # Bash 4.4, Zsh
    set -euo pipefail
else
    # Bash 4.3 and older chokes on empty arrays with set -u.
    set -eo pipefail
fi
shopt -s nullglob globstar

SCRIPT_ROOT="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )"
RUN_TOOL="${SCRIPT_ROOT}/run-tool.sh"
PCP_START="/home/mateus/workspace/PathConditionsProbability/restartServer"
INPUT_FILE="$1"

while IFS='' read -r LINE || [[ -n "$LINE" ]]; do
    echo "[run_batch] running ${LINE}"
	"$PCP_START" 9001
#	ps aux | grep java
	if  "$RUN_TOOL" aca "$LINE"; then
		echo "[run_batch] finished analysis for ${LINE} successfully."
	else
		echo "[run_batch] tool script returned non-zero error code. $?"
	fi
	killall java
	sleep 3
	ps aux | grep java
done < "$INPUT_FILE"
