module Statistics where

import Data.Time

data Statistics = Statistics {
  definiteTime  :: Double,
  definiteStart :: UTCTime,
  
  possibleTime  :: Double,
  possibleStart :: UTCTime,
  
  generalizeTime  :: Double,
  generalizeStart :: UTCTime,

  totalTime  :: Double,
  totalStart :: UTCTime,

  generalizeCount :: Int,
  iterationStat :: Int,

  partitionCount :: Int,
  exactPartitionCount :: Int
  } deriving (Show, Read)
