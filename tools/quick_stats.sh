#!/bin/bash

# This script collates the log results
# from runs of ALPACA over multiple
# remote nodes

LOGS="/localtmp/logs_alpaca"
GAPS=0
TOPS=0
EXACTS=0

# Doppio servers
S1="mjg6v@doppio01.cs.virginia.edu"
S2="mjg6v@doppio02.cs.virginia.edu"
S3="mjg6v@doppio03.cs.virginia.edu"
S4="mjg6v@doppio04.cs.virginia.edu"
S5="mjg6v@doppio05.cs.virginia.edu"

for server in $S1 $S2 $S3 $S4 $S5;
do
    tmpExacts=`ssh $server grep exact $LOGS/*_reach_*/exit.summary | wc -l`
    EXACTS=$(( EXACTS + tmpExacts ))

    tmpGaps=`ssh $server grep gap $LOGS/*_reach_*/exit.summary | wc -l`
    GAPS=$(( GAPS + tmpGaps ))

    tmpTops=`ssh $server grep top $LOGS/*_reach_*/exit.summary | wc -l`
    TOPS=$(( TOPS + tmpTops ))
done

echo "Exacts: $EXACTS"
echo "Gaps: $GAPS"
echo "Tops: $TOPS"
