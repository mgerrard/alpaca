#!/bin/bash

# This script collates the log results
# from runs of ALPACA over multiple
# remote nodes

LOGS="logs_alpaca"
LOGS_ZIP="logs_alpaca.zip"

timestamp=`date +"%Y-%m-%d_%H-%M-%S"`
LOCAL_LOGS="logs_$timestamp"
mkdir $LOCAL_LOGS

# Doppio servers
S1="mjg6v@doppio01.cs.virginia.edu"
S2="mjg6v@doppio02.cs.virginia.edu"
S3="mjg6v@doppio03.cs.virginia.edu"
S4="mjg6v@doppio04.cs.virginia.edu"
S5="mjg6v@doppio05.cs.virginia.edu"

for server in $S1 $S2 $S3 $S4 $S5;
do
    ssh $server <<EOF
      cd /localtmp
      rm -f $LOGS_ZIP
      rm -rf $LOGS/*/iter*
      zip -r $LOGS_ZIP $LOGS
EOF
    scp $server:/localtmp/$LOGS_ZIP .
    unzip $LOGS_ZIP && rm $LOGS_ZIP
    mv $LOGS/* $LOCAL_LOGS
    rm -rf $LOGS
done    
