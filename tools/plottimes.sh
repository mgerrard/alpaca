#!/bin/bash
if [ $# -ne 3 ];
    then echo "Expected three data files as parameters: gen_poss_def.dat first, then gen_poss.dat, then gen.dat"; exit

fi

GP_FILE="/tmp/gnuplot.in"

echo "unset xtics" >$GP_FILE
echo "set term pdfcairo dashed" >>$GP_FILE
echo "stats \"$1\" u 1:1 nooutput" >>$GP_FILE
echo "set xrange [*<-1:(STATS_records)<*]" >>$GP_FILE
echo "set yrange [*:(STATS_max_y+1)<*]" >>$GP_FILE
echo "set output 'stacked_times.pdf'" >>$GP_FILE
echo "set style data histogram" >>$GP_FILE
echo "set style histogram rowstacked" >>$GP_FILE
echo "plot \"$1\" with impulses lc 'yellow' lw 4 title 'Definite failure time', \"$2\" with impulses lc 'red' lw 4 title 'Possible failure time', \"$3\" with impulses lc 'light-gray' lw 4 title 'Generalization time'" >>$GP_FILE
gnuplot $GP_FILE
