#!/usr/bin/env bash
if test "$BASH" = "" || "$BASH" -uc "a=();true \"\${a[@]}\"" 2>/dev/null; then
    # Bash 4.4, Zsh
    set -euo pipefail
else
    # Bash 4.3 and older chokes on empty arrays with set -u.
    set -eo pipefail
fi
shopt -s nullglob globstar

SCRIPT_ROOT="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )"
CIVL_JAR="$SCRIPT_ROOT/civl.jar"
ACA_BIN="$SCRIPT_ROOT/aca"
LOG_FOLDER="${PWD}/logs"

TIMEOUT=3600
SEED=24026942

TOOL="$1"
SUBJECT="$2"
PCP_PORT="${3:-9001}"

usage() {
	echo "usage: ./run-experiments {tool} {c-program-source} {pcp-port (default=9001)}"
	echo "       where tool is in [aca,aca+dfs,aca+pse,dfs,pse]"
}

run_aca() {
	set -x
	declare -n RETURN=ACA_RESULT
	local INPUT_FILE
	local BASENAME
	local LOGFILE
	local INSTRUMENTED_SRC

	INPUT_FILE="$1"
	BASENAME="$(basename -s .c ${INPUT_FILE})"
	LOGFILE="${LOG_FOLDER}/${BASENAME}.aca.log"

	# buffering might delay the printing of the output
	if ! timeout -k 10 "$TIMEOUT" /usr/bin/time "$ACA_BIN" "$INPUT_FILE" -s 2>&1 | tee "$LOGFILE"; then
		echo "[run_aca] timeout (${TIMEOUT}s) triggered for subject ${INPUT_FILE}"
		return 1
	fi

	if grep -F "[aca:analysis] no gaps left to instrument" "$LOGFILE"; then
		echo "[run_aca] finished aca run without any gaps left to explore"
		return 2
	fi

	if grep -F "Trivial CFC; exiting." "$LOGFILE"; then
		echo "[run_aca] finished aca run with a trivial CFC"
		return 2
	fi
	
	if INSTRUMENTED_SRC="$(grep -F '[aca:instrumentGap] instrumented file:' ${LOGFILE} | cut -d ':' -f 3 )"; then
		echo "[run_aca] finished aca run. instrumented source at ${INSTRUMENTED_SRC}"
		RETURN="$INSTRUMENTED_SRC"
	else
		echo "[run_aca] couldn't find the instrumented source file path in ${LOGFILE}"
		return 1
	fi
	
	return 0
}

run_civl() {
	SEARCH="$1"
	INPUT_FILE="$2"
	BASENAME="$(basename -s .c ${INPUT_FILE})"
	LOGFILE="${LOG_FOLDER}/${BASENAME}.civl-${SEARCH}.log"

	case "$SEARCH" in
		pse)
			OPTIONS="-statistical -svcomp17 -saveStates=false -errorBound=1234567890 -seed=$SEED"
			;;
		dfs)
			OPTIONS="-svcomp17 -saveStates=false -errorBound=1234567890 -seed=$SEED"
			;;
		*)
			echo "Internal error: invalid civl search type: $SEARCH"
			exit 1
			;;
	esac
			
	timeout -k 10 "$TIMEOUT" /usr/bin/time java -jar "$CIVL_JAR" verify "$OPTIONS" "$INPUT_FILE" 2>&1 | tee "$LOGFILE"
}


#main
set -x
if [[ ! "$SUBJECT" || ! -f "$SUBJECT" ]]; then 
	echo "Error: Invalid or inexistent source file."
	usage
fi

mkdir -p "$LOG_FOLDER"

case "$TOOL" in
	aca)
		run_aca "$SUBJECT"
		echo "[run-tool:aca] finished solo aca run. ${ACA_RESULT}"
		;;
	aca+dfs)
		TIMEOUT=1800
		if run_aca "$SUBJECT"; then
			echo "aca finished, instrumented source can be found at: $ACA_RESULT"
			run_civl "dfs" "$ACA_RESULT"
		fi 
		;;
	aca+pse)
		TIMEOUT=1800
		if run_aca "$SUBJECT"; then
			echo "aca finished, instrumented source can be found at: $ACA_RESULT"
			run_civl pse "$ACA_RESULT"
		fi
		;;
	dfs)
		run_civl dfs "$SUBJECT"
		;;
	pse)
		run_civl pse "$SUBJECT"
		;;
	*)
		echo "Unknown tool combination: $TOOL"
		usage
		;;
esac

