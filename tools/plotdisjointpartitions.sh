#!/bin/bash
if [ $# -ne 2 ];
    then echo "Expected two data files as parameters: total_partitions.dat first, then partitions_without_gap.dat."; exit

fi

GP_FILE="/tmp/gnuplot.in"

echo "unset xtics" >$GP_FILE
echo "set term pdfcairo dashed" >>$GP_FILE
echo "stats \"$1\" u 1:1 nooutput" >>$GP_FILE
echo "set xrange [*<-1:(STATS_records)<*]" >>$GP_FILE
echo "set yrange [*:(STATS_max_y+1)<*]" >>$GP_FILE
echo "set output 'stacked_partitions.pdf'" >>$GP_FILE
echo "set style data histogram" >>$GP_FILE
echo "set style histogram rowstacked" >>$GP_FILE
echo "plot \"$1\" with impulses lc 'black' lw 3 title 'Gap in bounds', \"$2\" with impulses lc 'grey' lw 4 title 'Equivalent bounds'" >>$GP_FILE
gnuplot $GP_FILE
